<?php
/**
 * Template Name: Front Page Template
 *
 */

get_header();



$call_banner = get_field('call_banner');
$why_choose_us = get_field('why_choose_us');	
$help_heading = get_field('help_heading');	
$help_heading_2 = get_field('help_heading_2');	
$contact_section = get_field('contact_section');	
$testimonial = get_field('testimonial');	
$latest_news = get_field('latest_news');	







// print_r($why_choose_us);
?>
  
<div class="call-banner" style="background-image: url(<?php echo $call_banner['background_image']; ?>);">
	<div class="container">
		<h2><?php echo $call_banner['text']; ?></h2>
		<button class="btn"><a href="tel:<?php echo $contact_section['contact_number']; ?>">CALL NOW</a></button>
	</div>
</div>

<div class="choose-us">
	<div class="container">
		<div class="top-headings-section">
			<h2 class="text-center"><?php echo $why_choose_us['heading']; ?></span></h2>
			<p class="text-center"><?php echo $why_choose_us['content']; ?></p>
		</div>
		<div class="row">
			<?php

				if( have_rows('why_choose_us_blocks') ):
				    while ( have_rows('why_choose_us_blocks') ) : the_row();
			
						$image = get_sub_field('image');
						$content = get_sub_field('text');
						$link = get_sub_field('link');

	    	?>

				<div class="col-md-3 col-sm-6 ">
					<div class="choos">
						<a href="<?php echo $link; ?>">
							<figure><img src="<?php echo $image; ?>" alt=""></figure>
						</a>
						<div class="why-do">
							<a href="<?php echo $link; ?>"><h3 class="text-center"><?php echo $content; ?></h3></a>
						</div>
					</div>
				</div>
			
<?php
		endwhile;
		endif;
?>


		</div>
	</div>
</div>

<main class="home-help">
	<div class="container">
		<div class="top-headings-section">
			<h2 class="text-center"><?php echo $help_heading; ?></h2>
		</div>
		<div class="row help-1">
			
			<?php

				if( have_rows('help_blocks') ):
				    while ( have_rows('help_blocks') ) : the_row();
			
						$image = get_sub_field('image');
						$content = get_sub_field('text');

	    	?>

			<article class="col-md-4 col-sm-4 serv">
				<div class="art-1">
					<img src="<?php echo $image; ?>" alt="">
					<p><?php echo $content; ?></p>
				</div>
			</article>

<?php
		endwhile;
		endif;
?>

		</div>

		<div class="top-headings-section">
			<h2 class="text-center"><?php echo $help_heading_2; ?></h2>
		</div>
		<div class="row help-2">
			
			<?php

				if( have_rows('help_blocks_2') ):
					while ( have_rows('help_blocks_2') ) : the_row();

						$image = get_sub_field('image');
						$content = get_sub_field('text');

			?>

			<article class="col-md-4 col-sm-4 serv">
				<div class="art-1">
					<img src="<?php echo $image; ?>" alt="">
					<p><?php echo $content; ?></p>
				</div>
			</article>
<?php
		endwhile;
		endif;
?>

		</div>
	</div>
</main>

<!---contact Us Form end-->
    <div class="contact-form">
	  <div class="container">
	    <div class="top-headings-section">
			<h2 class="text-center"><?php echo $contact_section['contact_heading']; ?></h2>
		</div>
		
		
		<div class="row">
		  <div class="col-sm-8">
		   <?php echo do_shortcode('[contact-form-7 id="220" title="Home"]'); ?>
		  </div>
		  <div class="col-sm-4">
		     <div class="contact-no">
			   <a class="call" href="tel:<?php echo $contact_section['contact_number']; ?>"> <?php echo $contact_section['contact_number']; ?> &nbsp; &nbsp; <span> <i class="fa fa-phone" aria-hidden="true"></i> Call </span></a>
			 </div>
 		  </div>
		</div> 
	  </div>
	</div>
	
	
<!--- Contact Us Form end-->

	
<section class="testimonials" style="background: -webkit-linear-gradient(rgba(32, 51, 100, 0.9), rgba(32, 51, 100, 0.9)), url(<?php echo $testimonial['background_image']; ?>) no-repeat center center; background: linear-gradient(rgba(32, 51, 100, 0.9), rgba(32, 51, 100, 0.9)), url(<?php echo $testimonial['background_image']; ?>) no-repeat center center;">
	<div class="container">
		<div class="top-headings-section">
			<h2 class="text-center"><?php echo $testimonial['heading']; ?></h2>
		</div>
		<section class="regular slider">
	  
		<?php
			
			$args = array(
				'post_type'              => array( 'testimonial' ),
				'order'                  => 'DESC',
				'orderby'                => 'date',

			);

			$query = new WP_Query( $args );

			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					$query->the_post();
					
					$featured_img_url = get_the_post_thumbnail_url($post->ID,'full'); 
					$content = $post->post_content;
			?>
					<div class="t-slider">
					  <img src="<?php echo $featured_img_url; ?> ">
					  <p><?php echo $content; ?></p>
					  <h3><?php the_title(); ?></h3>
					  <h6><?php echo $post->post_excerpt; ?></h6>
					</div>


						<?php
				}
			} else {
				// no posts found
			}

			wp_reset_postdata();
		?>

	  </div>
	</div>

  </section>
</div>
</section>

<!-- START SECTION BLOG -->
<section class="blog">
	<div class="container">
		<div class="top-headings-section">
			<h2 class="text-center"><?php echo $latest_news['heading']; ?></h2>
			<p class="text-center"><?php echo $latest_news['content']; ?></p>
		</div>
		<div class="row">
			
		<?php
			
			$args = array(
				'post_type'              => array( 'post' ),
				'order'                  => 'DESC',
				'orderby'                => 'date',
				'posts_per_page'         => 3,

			);

			$query = new WP_Query( $args );

			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					$query->the_post();
					
					$featured_img_url = get_the_post_thumbnail_url($post->ID,'full'); 
					$content = $post->post_content;
					$post_date = get_the_date( 'F j, Y' );
					$comments_count = wp_count_comments($post->ID);
			?>


			<div class="col-md-4 col-sm-4 blog-pots hover-effect">
				<a href="#">
					<figure><img src="<?php echo $featured_img_url ?>" alt=""></figure>
				</a>
				<div class="blog-info">
					<h3><?php the_title(); ?></h3>
					<div class="date">
						<i class="fa fa-calendar" aria-hidden="true"></i>
						<p><?php echo $post_date; ?> | <i class="fa fa-user-o" aria-hidden="true"></i><?php the_author_nickname(); ?> | <i class="fa fa-comments-o" aria-hidden="true"></i><?php echo $comments_count->approved; ?></p>
					</div>
					<p><?php echo wp_trim_words( $content, 15, '...' ); ?></p>
					<a href="<?php the_permalink() ?>" class="btn btn-secondary">Read More...</a>
				</div>
			</div>

		<?php
				}
			} else {
				// no posts found
			}

			wp_reset_postdata();
		?>



		</div>
	</div>
</section>
<!-- END SECTION BLOG -->


<?php get_footer(); ?>