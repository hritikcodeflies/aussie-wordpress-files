<?php
/**
 * The Header template for our theme
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="icon" type="image/png" href="http://wp.localserverweb.com/aussie/wp-content/uploads/2019/03/favicon.png">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slick.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slick-theme.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i%7CPoppins:500,600,700" rel="stylesheet">
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php

$header_contact_number = get_field('header_contact_number', 'options');
$page_featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 


?>
	<div class="header-top">
		<div class="container">
			<div class="top-info">
				<div class="call-header">
					<p><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:<?php echo $header_contact_number; ?>"> <?php echo $header_contact_number; ?></a></p>
				</div>
				<!-- <div class="address-header">
					<p><i class="fa fa-map-marker" aria-hidden="true"></i> 95 South Park Ave, AUS</p>
				</div>
				<div class="mail-header">
					<p><i class="fa fa-envelope" aria-hidden="true"></i> info@aussiedebtrelief.com.au</p>
				</div> -->
			</div>
			
			<!-- <div class="top-social">
				<div class="social-icons-header">
					<div class="social-icons">
						<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa fa-linkedin" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a>
					</div>
				</div>
			</div> -->
		</div>
	</div>
  
	<nav class="navbar">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>                        
				</button>
				<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php header_image(); ?>" class="img-responsive">
				</a>
				<div class="call-nav">
					<p><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:<?php echo $header_contact_number; ?>"> <?php echo $header_contact_number; ?></a></p>
				</div>
			</div>

			<div class="collapse navbar-collapse" id="myNavbar">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'items_wrap' => '<ul class="nav navbar-nav navbar-right">%3$s</ul>', 'menu_class' => 'nav-menu' ) ); ?>
			</div>
		</div>
	</nav>
	
<?php if ( is_front_page() ) : 

$main_banner = get_field('main_banner');

?>
	<div class="banner" style="	background: url(<?php echo $page_featured_img_url; ?>) no-repeat;
">
		<div class="banner-text-left">
			<?php echo $main_banner['left_side_text']; ?>
		</div>
		<div class="banner-text">
			
			<!-- <p>Take Action Call us NOW !</p>
			<a href="tel:+6494461709"><h1> <i class="fa fa-phone" aria-hidden="true"></i> 1300 861 455</h1></a> -->
			<?php echo $main_banner['right_side_text']; ?>
		</div>
	</div>
	
<?php else : ?>
	<div class="inner-bannrbox">
		<img src="<?php if($page_featured_img_url){echo $page_featured_img_url; } else { echo get_template_directory_uri(); ?>/img/family-slider-sbs-solicitors.jpg <?php } ?>">
		<div class="textbox">
			<div class="container">
				<div class="row">
				
				
				
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
							<?php if(function_exists('bcn_display'))
								{
									bcn_display();
								}?>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>	
<?php endif; ?>