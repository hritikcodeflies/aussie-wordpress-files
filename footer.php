<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
$facebook_url = get_field('facebook', 'options');
$twitter_url = get_field('twitter', 'options');
$google_plus_url  = get_field('google_plus', 'options');
$pinterest_url = get_field('pinterest', 'options');
$youtube_url = get_field('youtube', 'options');


$copyright_text = get_field('copyright_text', 'options');
$footer_background_image = get_field('footer_background_image', 'options');
?>

 <!-- footer-->
 <footer class="site-footer footer-map" style="background-image: url(<?php echo $footer_background_image; ?>);">
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-3 animated v fadeInRight">
          <?php echo dynamic_sidebar('sidebar-11'); ?>

          
          <h5>Social Media</h5>
          <hr>
          <ul class="social-icons">
            <li><a href="<?php echo $facebook_url; ?>"><i class="fa fa-facebook"></i></a></li>
            <li><a href="<?php echo $twitter_url; ?>"><i class="fa fa-twitter"></i></a></li>
            <li><a href="<?php echo $google_plus_url; ?>"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="<?php echo $pinterest_url; ?>"><i class="fa fa-pinterest-p"></i></a></li>
            <li><a href="<?php echo $youtube_url; ?>"><i class="fa fa-youtube-play"></i></a></li>
          </ul>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3 animated v fadeInRight">
          <h5>Recent News</h5>
          <hr>
          <ul class="widget-news-simple">
            
        			     
        <?php
              
              $args = array(
                'post_type'              => array( 'post' ),
                'order'                  => 'DESC',
                'orderby'                => 'date',
                'posts_per_page'         => 2,

              );

              $query = new WP_Query( $args );

              if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                  $query->the_post();
                  
                  $featured_img_url = get_the_post_thumbnail_url($post->ID,'full'); 
                  $content = $post->post_content;
                  $post_date = get_the_date( 'jS M, Y' );
                  $comments_count = wp_count_comments($post->ID);
        ?>

           <li>
              <div class="news-thum"><a href="<?php the_permalink() ?>"><img src="<?php echo $featured_img_url; ?>" alt="new-thum-1"></a></div>
              <div class="news-text-thum">
                <h6><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h6>
                <p><?php echo wp_trim_words( $content, 7, '...' ); ?></p>
                <div><?php echo $post_date; ?> </div>
              </div>
            </li>
			
			
            <?php
                }
              } else {
                // no posts found
              }

              wp_reset_postdata();
            ?>        
			
			
          </ul>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3 animated v fadeInRight">
          <h5>Useful links</h5>
          <hr>
		   <?php 
              // echo dynamic_sidebar('sidebar-13');
           wp_nav_menu( array('menu' => 'Footer Useful links', 'items_wrap' => '<ul class="use-slt-link">%3$s</ul>')); ?>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3 animated v fadeInRight">
          <?php echo dynamic_sidebar('sidebar-14'); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 animated v fadeInRight">
          <p class="text-xs-center"><?php echo $copyright_text; ?></p>
        </div>
        <a href="#" id="scroll-up" class="animated v fadeInUp"><i class="fa fa-arrow-up"></i></a> </div>
    </div>
  </div>
</footer>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(".slider").slick({

  // normal options...
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 2,

  // the magic
  responsive: [{

      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        dots: true
      }

    }, {

      breakpoint: 300,
      settings: "unslick" // destroys slick

    }]
});
</script>


<?php wp_footer(); ?>
</body>
</html>